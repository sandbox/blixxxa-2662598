<?php
/**
 * @file
 * Contains \Drupal\search_exclude\Routing\RouteSubscriber.
 */

namespace Drupal\restrict_node\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('entity.node.canonical')) {
      $route->setRequirement('_access_restrict_node', 'TRUE');
    }
  }
}
