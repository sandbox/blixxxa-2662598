<?php

/**
 * @file
 * Contains \Drupal\restrict_node\Access\RestrictNodeAccessCheck.
 */

namespace Drupal\restrict_node\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\node\NodeInterface;

/**
 * Determines whether the requested node can be viewed.
 */
class RestrictNodeAccessCheck implements AccessInterface {

  /**
   * Constructs a RestrictNodeAccessCheck object.
   */
  public function __construct() {
  }

  /**
   * Checks access for viewing the node.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node requested to be viewed.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(NodeInterface $node) {
    $type = $node->bundle();
    $user = \Drupal::currentUser();

    if ($node->status && $user->hasPermission("view $type published content")) {
      return AccessResult::allowed();
    }

    return AccessResult::forbidden();
  }
}
